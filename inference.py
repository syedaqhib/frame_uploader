#!/usr/bin/python3
# -*- coding: utf-8 -*-
# pylint: disable=C0103
# pylint: disable=E1101

import sys
import time
import numpy as np
import tensorflow as tf
import cv2
from os import listdir
from os.path import isfile, join
import os
import boto3
import multiprocessing

sys.path.append("..")

from utils import label_map_util
from utils import visualization_utils_color as vis_util

def s3up(cropped_file_name, bucket_name):
    print("Starting upload of cropped image : " + cropped_file_name)
    s3.upload_file(cropped_file_name, bucket_name, cropped_file_name)
    print("Finishing upload of frame number : " + cropped_file_name)

def load_image_into_numpy_array(image):
  (im_width, im_height, im_depth) = image.shape
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


jobs = []
s3 = boto3.client('s3')
bucket_name = "rtspframestorage"
file_name = ""

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = 'frozen_inference_graph_face.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = 'face_label_map.pbtxt'

NUM_CLASSES = 1

IMG_DIR = "vid_frames/"

WRITE_DIR = "annotated_media/"
CROPPED_WRITE_DIR = "cropped_images/"

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

file_list = [f for f in listdir(IMG_DIR) if isfile(join(IMG_DIR, f)) and (f.endswith('.jpeg') or f.endswith('.jpg'))]
num_files = len(file_list)
if num_files == 0:
    print("No training files found")
    exit(-1)

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.compat.v1.GraphDef()
    with tf.io.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

with detection_graph.as_default():
  config = tf.compat.v1.ConfigProto()
  config.gpu_options.allow_growth = True
  with tf.compat.v1.Session(graph=detection_graph, config=config) as sess:
    frames_left = num_files
    while frames_left:
      frames_left -= 1
      image_index = num_files - frames_left - 1
      image = cv2.imread(os.path.join(IMG_DIR, file_list[image_index]))
    #   if out is None:
    #       [h, w] = image.shape[:2]
    #       out = cv2.VideoWriter("./media/test_out.avi", 0, 25.0, (w, h))


      image_np = image
      (im_width, im_height, im_depth) = image.shape
      # the array based representation of the image will be used later in order to prepare the
      # result image with boxes and labels on it.
      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
      image_np_expanded = np.expand_dims(image_np, axis=0)
      image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
      # Each box represents a part of the image where a particular object was detected.
      boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
      # Each score represent how level of confidence for each of the objects.
      # Score is shown on the result image, together with the class label.
      scores = detection_graph.get_tensor_by_name('detection_scores:0')
      classes = detection_graph.get_tensor_by_name('detection_classes:0')
      num_detections = detection_graph.get_tensor_by_name('num_detections:0')
      # Actual detection.
      start_time = time.time()
      (boxes, scores, classes, num_detections) = sess.run(
          [boxes, scores, classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})
      #print(scores)

      # for i in range(100):
      #   if scores[0][i] > 0.5:
      #     ymin = int(boxes[0,i,0] * im_height)
      #     xmin = int(boxes[0,i,1] * im_width)
      #     ymax = int(boxes[0,i,2] * im_height)
      #     xmax = int(boxes[0,i,3] * im_width)
      #     print("ymin : " + str(ymin))
      #     print("xmin : " + str(xmin))
      #     print("ymax : " + str(ymax))
      #     print("xmax : " + str(xmax))
      #     cropped_image = image[ymin:ymax, xmin:xmax]
      #     # cropped_image_tensor = tf.image.crop_to_bounding_box(image, ymin, xmin, 
      #     #                              ymax - ymin, xmax - xmin)
      #     # cropped_image = sess.run(cropped_image_tensor)
      #     split_file = file_list[image_index].split('.')
      #     #print(split_file)
      #     print("Writing cropped image to " + os.path.join(CROPPED_WRITE_DIR, split_file[0] + '_' + str(i) + '.' + split_file[1]))
      #     cv2.imwrite(os.path.join(CROPPED_WRITE_DIR, split_file[0] + '_' + str(i) + '.' + split_file[1]), cropped_image)
    
      num_boxes = 0
      for i in range(100):
        if scores[0][i] > 0.5:
          num_boxes += 1
      cropped = tf.image.crop_and_resize(
              image_np_expanded,
              boxes[0][:num_boxes],
              box_ind = np.zeros(num_boxes),
              crop_size = [300,300]
              )
      cropped = sess.run(cropped)

      for i in range(num_boxes):
          split_file = file_list[image_index].split('.')
          cropped_image = cropped[i]
          cropped_file_name = os.path.join(CROPPED_WRITE_DIR, split_file[0] + '_' + str(i) + '.' + split_file[1])
          print("Writing cropped image to " + os.path.join(CROPPED_WRITE_DIR, split_file[0] + '_' + str(i) + '.' + split_file[1]))
          cv2.imwrite(os.path.join(CROPPED_WRITE_DIR, split_file[0] + '_' + str(i) + '.' + split_file[1]), cropped_image)
          p = multiprocessing.Process(target = s3up, args = (cropped_file_name, bucket_name))
          jobs.append(p)
          p.start()
      
      for p in jobs:
        p.join()  

      #print(cropped.shape)
      

      elapsed_time = time.time() - start_time
      print('inference time cost: {}'.format(elapsed_time))
      #print(boxes.shape, boxes)
      #print(scores.shape,scores)
      #print(classes.shape,classes)
      #print(num_detections)
      # Visualization of the results of a detection.
      vis_util.visualize_boxes_and_labels_on_image_array(
#          image_np,
          image,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=1)
      print("Writing annotated image to " + os.path.realpath(file_list[image_index]))
      cv2.imwrite(os.path.join(WRITE_DIR, file_list[image_index]), image)