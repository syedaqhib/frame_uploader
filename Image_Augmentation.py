#!/usr/bin/env python
# coding: utf-8

# In[53]:


import Augmentor
p = Augmentor.Pipeline("/Users/nupuragrawal/desktop/facial_images/")
# Point to a directory containing ground truth data.
# Images with the same file names will be added as ground truth data
# and augmented in parallel to the original data.

# Add operations to the pipeline as normal:
p.random_distortion(probability=1, grid_width=4, grid_height=4, magnitude=8)
p.rotate90(probability=0.5)
p.rotate270(probability=0.5)
p.skew_left_right(probability=0.3)
p.skew_top_bottom(probability=0.3)
p.shear(probability=0.3)
p.random_distortion(probability=0.1,grid_width =10 , grid_height=10, magnitude=10)
p.flip_left_right(probability=0.8)
p.flip_top_bottom(probability=0.3)
p.zoom_random(probability=0.5, percentage_area=0.8)
p.gaussian_distortion(probability=0.2, grid_width =10 , grid_height=10, magnitude=10, corner="ul", method="in", mex=0.5, mey=0.5, sdx=0.05, sdy=0.05)
p.sample(10)


# In[59]:


import csv
import pandas as pd 
import os
data = pd.read_csv("/Users/nupuragrawal/desktop/facial_expressions/data/legend.csv") 
data.head()


# In[61]:


list_augmented=list(os.listdir("/Users/nupuragrawal/desktop/facial_images/output"))


# In[62]:


list_original=list(os.listdir("/Users/nupuragrawal/desktop/facial_images/"))


# In[72]:


list_augnames=[]
for i in range(len(list_augmented)):
    list_augnames.append(list_augmented[i][23:][:len(list_augmented[i][23:])-41])


# In[88]:


df = pd.DataFrame(list_augmented) 
df['image']=list_augnames
df2 = pd.merge(df, data, on='image', how='left')
df2=df2.drop(columns=['image', 'user.id'])
df2_columns = ["image", "emotion"]
df2.columns = df2_columns
df3 = data.append(df2, sort=False)
df3=df3.drop(columns=['user.id'])
df3['emotion']=df3['emotion'].str.lower()
df3


# In[89]:


#from PIL import Image
#import os, sys

#path = "/Users/nupuragrawal/desktop/facial_images/output"
#dirs = os.listdir( path )

#def resize():
    #for item in dirs:
        #if os.path.isfile(path+item):
            #im = Image.open(path+item)
            #f, e = os.path.splitext(path+item)
            #imResize = im.resize((200,200), Image.ANTIALIAS)
            #imResize.save(f + ' resized.jpg', 'JPEG', quality=90)

#resize()


# In[ ]:




