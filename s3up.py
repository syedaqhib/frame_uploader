# Syed Aqhib Ahmed

import cv2
import os
import boto3
import numpy as np
import multiprocessing
import queue

#BUFFER_SIZE = 200

def compare_images(img1, img2):
    img1 = normalize(img1)
    img2 = normalize(img2)
    diff = np.subtract(img1,img2)
    m_norm = np.sum(abs(diff), axis = 0)
    m_norm = np.sum(m_norm)
    return (m_norm)
 
#normalizing images
def normalize(arr):
    amin = np.amin(arr)
    rng = np.amax(arr) - amin
    
    return (np.subtract(arr,amin))*255/rng

def write_image(write_path, img):
    cv2.imwrite(write_path, img)

def s3up(count, bucket_name):

    file_name = "/vid_frames/frame" + str(count) + ".jpg"
    print("Starting upload of frame number : " + str(count))
    s3.upload_file(file_name, bucket_name, file_name)
    print("Finishing upload of frame number : " + str(count))


#####Script#####

jobs = []
s3 = boto3.client('s3')
bucket_name = "rtspframestorage"
file_name = ""

#feed_url = "rtsp://vinpat123:vinpat123@192.168.1.201/live"
feed_url = "rtsp://184.72.239.149/vod/mp4:BigBuckBunny_115k.mov"
vc = cv2.VideoCapture(feed_url)

#cv2.namedWindow("Preview")
 
if vc.isOpened(): # try to get the first frame
    rval, frame_prev = vc.read() #Start with frame0
    rval, frame_cur = vc.read() #Start with frame1
else:
    rval = False
    print("Failed to opern stream")

count = 1

write_path = os.path.join('/vid_frames/' ,"frame0.jpg")
cv2.imwrite(write_path, frame_prev)

file_name = "/vid_frames/frame0.jpg"
print("Starting upload of frame number : 0")
#s3.upload_file(file_name, bucket_name, file_name)
print("Finishing upload of frame number : 0")

while rval:

    #cv2.imshow("preview", frame_prev)
    key=cv2.waitKey(20)

    if key == 27: # exit on ESC
        break

    if rval == False:
         print("Frame Skipped or end of feed")
         break

    else:
        gray1 = cv2.cvtColor(frame_prev, cv2.COLOR_BGR2GRAY)
        gray2 = cv2.cvtColor(frame_cur, cv2.COLOR_BGR2GRAY)
        n_m = compare_images(gray1,gray2)
        print("")
        print("Comparison Value : ", n_m)
        print("")
        print("Frame number %d for feed at - " %count + feed_url)

        if n_m > 180000:
            write_path = os.path.join('/vid_frames/' ,"frame%d.jpg" % count)
            write_image(write_path, frame_cur)
            p = multiprocessing.Process(target = s3up, args = (count, bucket_name))
            jobs.append(p)
            p.start()
        frame_prev = frame_cur
        rval, frame_cur = vc.read()
    count += 1

for p in jobs:
    p.join()
vc.release()
#cv2.destroyWindow("preview")